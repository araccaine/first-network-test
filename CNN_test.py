import tensorflow as tf
from tensorflow import keras

import numpy as np
import os


# image dimensions used in NN
img_width = 50
img_height = 50

# information about training data
class_names = ['Fenster', 'Tür']


# load saved keras model
load_path = 'model_save/'
model = tf.keras.models.load_model(
    load_path, custom_objects=None, compile=True, options=None
)

print(model.summary())

# get test images and test them
images_dir = 'testimages/'

for file in os.scandir(images_dir):
    if file.is_file() and file.name.endswith(('.jpg', '.jpeg', '.png', '.PNG')) :
        image_path = os.path.join(images_dir, file.name)
        # print(image_path)

        img = keras.preprocessing.image.load_img(
              image_path,
              color_mode='grayscale',
              target_size=(img_width, img_height)
              )

        img_array = keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0)    # Create a batch

        predictions = model.predict(img_array)
        score = tf.nn.softmax(predictions[0])


        print(
            "The image '{}' most likely belongs to '{}' with a {:.2f} percent confidence."
            .format(image_path, class_names[np.argmax(score)], 100 * np.max(score))
        )
        print("Scores: ", score, "\n")
    else:
        print("{} is not an image!\n".format(file))