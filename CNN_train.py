import numpy as np

import tensorflow as tf
from tensorflow import keras

from functools import partial

import datetime


# create datasets from folders with images with keras
# https://www.tensorflow.org/tutorials/load_data/images?hl=en
image_path = 'images/'
val_split = 0.2
batch_size = 4
img_height = 50
img_width = 50

own_seed = 234

num_epochs = 150

color_mode = 'grayscale'
if color_mode == 'rgb':
    num_channels = 3
if color_mode == 'grayscale':
    num_channels = 1
else:
    print('Color mode error! color_mode must be grayscale or rgb')
    exit()

# configure training and validation set
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
            image_path,
            color_mode=color_mode,
            validation_split=val_split,
            subset="training",
            seed=own_seed,
            #image_size=(img_height, img_width),
            batch_size=batch_size)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
            image_path,
            color_mode=color_mode,
            validation_split=val_split,
            subset="validation",
            seed=own_seed,
            #image_size=(img_height, img_width),
            batch_size=batch_size)

print('class names:', train_ds.class_names)
num_classes = len(train_ds.class_names)


# Configure Dataset for performance
# https://www.tensorflow.org/tutorials/load_data/images?hl=en#configure_dataset_for_performance
AUTOTUNE = tf.data.experimental.AUTOTUNE


def configure_for_performance(ds):
    """
    Returns a shuffled and batched dataset with caching to make them as fast as possible
    :param ds:  keras dataset
    :return:    shuffled and batched keras dataset
    """
    ds = ds.cache()
    ds = ds.shuffle(buffer_size=1000)
    # ds = ds.batch(batch_size) # if no batch_size is specified above in preprocessing
    ds = ds.prefetch(buffer_size=AUTOTUNE)
    return ds


train_ds = configure_for_performance(train_ds)
val_ds = configure_for_performance(val_ds)

#train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
#val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

# Create and compile CNN model

# preprocessing
# https://www.tensorflow.org/api_docs/python/tf/keras/layers/experimental/preprocessing
resize_and_rescale = tf.keras.Sequential([
    keras.layers.experimental.preprocessing.Resizing(img_height, img_width),
    keras.layers.experimental.preprocessing.Rescaling(1./255),
])

# https://keras.io/api/layers/preprocessing_layers/image_preprocessing/random_rotation/
data_augmentation = tf.keras.Sequential([
  keras.layers.experimental.preprocessing.RandomFlip("horizontal_and_vertical"),
  keras.layers.experimental.preprocessing.RandomRotation(1.0),
])

DefaultConv2D = partial(keras.layers.Conv2D,
                        kernel_size=3, activation='elu', padding="same")

# assemble model
model = keras.models.Sequential([
    resize_and_rescale,
    data_augmentation,
    DefaultConv2D(filters=4, kernel_size=3),# add input_shape=[image_width, image_height, num_of_channels] if this is the first layer
    keras.layers.Dropout(0.5),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Dropout(0.5),
    DefaultConv2D(filters=8),
    keras.layers.Dropout(0.5),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Dropout(0.5),
    DefaultConv2D(filters=16),
    keras.layers.Dropout(0.5),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Flatten(),
    keras.layers.Dense(units=16, activation='elu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(units=num_classes, activation='softmax'),
])

# custom optimizer:
cust_opt = tf.keras.optimizers.Nadam(
    learning_rate=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-07,
    name='Nadam'
)

# test with another optimizer:
# cust_opt = tf.keras.optimizers.RMSprop(
            # learning_rate=0.0001, rho=0.9, momentum=0.0, epsilon=1e-07, centered=True,
            # name='RMSprop'
# )

# compile model
model.compile(loss="sparse_categorical_crossentropy",
              optimizer=cust_opt,
              metrics=["accuracy"])

# building and summary of the model
# in https://www.tensorflow.org/js/guide/models_and_layers#model_summary it is mentioned that the shape (None, ...)
# means that the batch-size can be flexible
model.build([None, img_width, img_height, num_channels])

print(model.summary())


# Train CNN model

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

# use callbacks for logging and early stopping
my_callbacks = [tf.keras.callbacks.TensorBoard(log_dir=log_dir, write_graph=True, histogram_freq=10),
                tf.keras.callbacks.EarlyStopping(patience=50, restore_best_weights=True) ]

# start training
history = model.fit(train_ds,
                    epochs=num_epochs,
                    validation_data=val_ds,
                    callbacks=my_callbacks,
                    )

# save CNN model
# https://keras.io/api/models/model_saving_apis/
save_path = 'model_save/'
model.save(
    save_path,
    overwrite=False,
    include_optimizer=True,
    )
