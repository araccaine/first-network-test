# First Network Test

This is a very basic and bad performing Convolutional Neuronal Network for detecting symbols used in floor plans.

Training images and test images made by [Pablo Baeza](https://gitlab.com/pablobaeza).

# Usage
Start the Training with 
```
$ python CNN_train.py
```

Start the evaluation with

```
$ python CNN_test.py
```

# Used Net Architekture

    _________________________________________________________________
    Layer (type)                 Output Shape              Param #
    =================================================================
    sequential (Sequential)      (None, 50, 50, 1)         0
    _________________________________________________________________
    sequential_1 (Sequential)    (None, 50, 50, 1)         0
    _________________________________________________________________
    conv2d (Conv2D)              (None, 50, 50, 4)         40
    _________________________________________________________________
    max_pooling2d (MaxPooling2D) (None, 25, 25, 4)         0
    _________________________________________________________________
    conv2d_1 (Conv2D)            (None, 25, 25, 8)         296
    _________________________________________________________________
    max_pooling2d_1 (MaxPooling2 (None, 12, 12, 8)         0
    _________________________________________________________________
    conv2d_2 (Conv2D)            (None, 12, 12, 16)        1168
    _________________________________________________________________
    max_pooling2d_2 (MaxPooling2 (None, 6, 6, 16)          0
    _________________________________________________________________
    flatten (Flatten)            (None, 576)               0
    _________________________________________________________________
    dense (Dense)                (None, 16)                9232
    _________________________________________________________________
    dropout (Dropout)            (None, 16)                0
    _________________________________________________________________
    dense_1 (Dense)              (None, 2)                 34
    =================================================================
    Total params: 10,770
    Trainable params: 10,770
    Non-trainable params: 0
    _________________________________________________________________


# Result:
Note: `f**.png` are windows ("Fenster"), `t**.png` are doors ("Tür").

    The image 'testimages/f01.PNG' most likely belongs to 'Fenster' with a 72.64 percent confidence.
    Scores:  tf.Tensor([0.7264262  0.27357388], shape=(2,), dtype=float32)

    The image 'testimages/f02.PNG' most likely belongs to 'Fenster' with a 73.10 percent confidence.
    Scores:  tf.Tensor([0.73101926 0.26898074], shape=(2,), dtype=float32)

    The image 'testimages/f03.PNG' most likely belongs to 'Fenster' with a 73.11 percent confidence.
    Scores:  tf.Tensor([0.73105836 0.26894164], shape=(2,), dtype=float32)

    The image 'testimages/f04.PNG' most likely belongs to 'Fenster' with a 73.11 percent confidence.
    Scores:  tf.Tensor([0.7310544  0.26894566], shape=(2,), dtype=float32)

    The image 'testimages/t01.PNG' most likely belongs to 'Tür' with a 64.70 percent confidence.
    Scores:  tf.Tensor([0.35303482 0.64696515], shape=(2,), dtype=float32)

    The image 'testimages/t02.png' most likely belongs to 'Fenster' with a 73.08 percent confidence.
    Scores:  tf.Tensor([0.7308342 0.2691658], shape=(2,), dtype=float32)

    The image 'testimages/t03.png' most likely belongs to 'Tür' with a 73.08 percent confidence.
    Scores:  tf.Tensor([0.26920387 0.7307961 ], shape=(2,), dtype=float32)

    The image 'testimages/t04.PNG' most likely belongs to 'Tür' with a 68.00 percent confidence.
    Scores:  tf.Tensor([0.3200458  0.67995423], shape=(2,), dtype=float32)